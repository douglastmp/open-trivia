import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://open-trivia-256116.firebaseio.com/'
});

instance.defaults.headers.common['Authorization'] = 'AIzaSyDSEhQEFuxvTElcoTzuNlyXMQ-I_c7p2Wg';

export default instance;