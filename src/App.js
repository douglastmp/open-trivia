import React from 'react';
import Header from './components/Header'
import Categories from './components/Categories';
import Trivia from './containers/Trivia'
import {BrowserRouter, Route, Switch } from 'react-router-dom';
import Result from './components/Result';

function App() {
  return (
    <div className="App">
      <Header/>
      <BrowserRouter>
        <Switch>
          <Route exact path="/result" render={() => (<Result/>) } />
          <Route exact path="/trivia" render={() => (<Trivia/>) } />
          <Route exact path="/" render={() => (<Categories/>) } />
          <Route render={() => <h1>Not found</h1>}/>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
