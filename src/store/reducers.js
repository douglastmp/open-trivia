import * as actionTypes from './actions';

const initialState = {
    categoryId:0,
    currentQuestion: '',
    currentOptions: [],
    questionNumber:1,
    correctEasyQuestion:0,
    incorrectEasyQuestion:0,
    correctMediumQuestion:0,
    incorrectMediumQuestion:0,
    correctHardQuestion:0,
    incorrectHardQuestion:0,
    lastCorrectAnswer:null,
    difficulty:'medium'
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.CURRENT_CATEGORY:
            return{
                ...state,
                categoryId:action.categoryData.idCategory
            }
        case actionTypes.CURRENT_QUESTION:
            return{
                ...state,
                currentQuestion:action.questionData.question,
                currentOptions:action.questionData.options     
            }
        case actionTypes.RAISE_DIFFICULTY:
            let raiseDifficulty = ''
            if (state.difficulty === 'easy') {    
                raiseDifficulty = 'medium'
            }
            else if (state.difficulty === 'medium'){
                raiseDifficulty = 'hard'
            }
            else{
                raiseDifficulty = 'hard'
            }
            return{
                ...state,
                difficulty:raiseDifficulty
            }
        case actionTypes.LOWER_DIFFICULTY:
            let lowerDifficulty = ''
            if (state.difficulty === 'hard') {                
                lowerDifficulty = 'medium' 
            } else if (state.difficulty === 'medium') {
                lowerDifficulty = 'easy' 
            }        
            else{
                lowerDifficulty = 'easy' 
            }
            return{
                ...state,
                lastCorrectAnswer:null,
                difficulty:lowerDifficulty
            }    
        case actionTypes.CORRECT_QUESTION:
            if (state.difficulty === 'easy') {
                return{
                    ...state,
                    questionNumber: state.questionNumber + 1,
                    correctEasyQuestion: state.correctEasyQuestion + 1,
                    lastCorrectAnswer: true
                }
            }
            else if (state.difficulty === 'medium'){
                return{
                    ...state,
                    questionNumber: state.questionNumber + 1,
                    correctMediumQuestion: state.correctMediumQuestion + 1,
                    lastCorrectAnswer: true
                }    
            } 
            else if (state.difficulty === 'hard') {
                return{
                    ...state,
                    questionNumber: state.questionNumber + 1,
                    correctHardQuestion: state.correctHardQuestion + 1,
                    lastCorrectAnswer: true
                }    
            }
            return {
                ...state
            }
        case actionTypes.INCORRECT_QUESTION:
            if (state.difficulty === 'easy') {
                return{
                    ...state,
                    questionNumber: state.questionNumber + 1,
                    incorrectEasyQuestion: state.incorrectEasyQuestion + 1,
                    lastCorrectAnswer: false
                }
            } else if (state.difficulty === 'medium') {
                return{
                    ...state,
                    questionNumber: state.questionNumber + 1,
                    incorrectMediumQuestion: state.incorrectMediumQuestion + 1,
                    lastCorrectAnswer: false
                }    
            } else if (state.difficulty === 'hard') {
                return{
                    ...state,
                    questionNumber: state.questionNumber + 1,
                    incorrectHardQuestion: state.incorrectHardQuestion + 1,
                    lastCorrectAnswer: false
                }    
            }
            return {
                ...state
            }               
        case actionTypes.EXIT:
            return {
                ...state,
                categoryId:0,
                currentQuestion:'',
                currentOptions: [],
                questionNumber:1,
                correctEasyQuestion:0,
                incorrectEasyQuestion:0,
                correctMediumQuestion:0,
                incorrectMediumQuestion:0,
                correctHardQuestion:0,
                incorrectHardQuestion:0,
                lastCorrectAnswer:null,
                difficulty:'medium'
            }
            
        default:
            break;
    }

    return state;
};

export default reducer;
