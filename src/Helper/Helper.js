import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from '@fortawesome/free-solid-svg-icons'


const ReturnDifficultyHtml = (difficulty) => {
    switch (difficulty) {
        case 'easy':
            return <span className="badge badge-pill badge-dark w-100"><FontAwesomeIcon icon={faStar}/> Easy </span>
        case 'medium':
            return <span className="badge badge-pill badge-dark w-100"><FontAwesomeIcon icon={faStar}/><FontAwesomeIcon icon={faStar}/> Medium </span>    
        case 'hard': 
            return <span className="badge badge-pill badge-dark w-100"><FontAwesomeIcon icon={faStar}/><FontAwesomeIcon icon={faStar}/><FontAwesomeIcon icon={faStar}/> Hard </span>            
        default:
            return <span></span>;
            
    }

    
}


export default ReturnDifficultyHtml