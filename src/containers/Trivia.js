/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {useEffect, useRef, useState} from 'react'
import * as actionTypes from '../store/actions'
import { connect } from 'react-redux'
import Axios from 'axios'
import myAxios from '../services/axios'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimesCircle, faCheckCircle, faArrowRight} from '@fortawesome/free-solid-svg-icons'
import ReturnDifficultyHTML from '../Helper/Helper'
import '../styles/css/Trivia.css'
import Modal from 'react-awesome-modal';
import {useHistory  , withRouter } from 'react-router-dom'

const Trivia = (props) => {
    let history = useHistory()
    let answerOptions = useRef([])
    let correctAnswer = useRef('')
    let categoryName = useRef('')
    let difficultyHTML = useRef('')
    let questionHTML = useRef('')
    const [selectedAnswer, setSelectedAnswer] = useState(0);
    const [answer, setAnswer] = useState(0);
    const [visibleModal, setVisibleModal] = useState(false);    
    const [isCorrectAnswer, setIsCorrectAnswer] = useState(false);    

    const exitHandler = () => {
        props.exitTrivia()
    }
    
    const answerSelectionHandler = (index) => {
        setSelectedAnswer(index + 1)
    }

    const feedbackHandler = () => {
        if (selectedAnswer > 0) {
            console.log("conferindo")
            setAnswer(selectedAnswer)
            setSelectedAnswer(0);
            setIsCorrectAnswer(String(answerOptions.current[selectedAnswer - 1]).trim().replace(/\s/g, '') === correctAnswer.current.trim().replace(/\s/g, ''))
            answerHandler()
        }
    }

    const answerHandler = () => {   
        if (isCorrectAnswer === true) {
            console.log('correto')
            if (props.lastCorrect === true) {
                props.raiseDifficulty()    
                props.correctQuestion()
            } else
            {
                props.correctQuestion()
            }  
        } 
        else {       
            console.log('incorreto')
            if (props.lastCorrect === false) {
                props.incorrectQuestion()
                props.lowerDifficulty()    
            } else {
                props.incorrectQuestion()
            }   
        }
        console.log("corrigiu")
    }

    const openModal = () => {
        setVisibleModal(true)
    }

    const closeModal = () => {
        setVisibleModal(false)
    }

    const getNextQuestion = () => {
        Axios.get(
            `https://opentdb.com/api.php?amount=1&category=${props.category}&difficulty=${props.difficulty}&type=multiple`
        ).then((res) => {
            answerOptions.current = res.data.results[0].incorrect_answers
            answerOptions.current.splice(Math.floor(Math.random() * 3), 0, res.data.results[0].correct_answer)
            correctAnswer.current = res.data.results[0].correct_answer
            categoryName.current = res.data.results[0].category
            difficultyHTML.current = ReturnDifficultyHTML(res.data.results[0].difficulty)
            questionHTML.current = res.data.results[0].question
            props.updateQuestion(res.data.results[0].question, answerOptions.current)
        })        
    }

    useEffect(() => {
        console.log('checou')
        if (props.questionNumber <= 10){
            let data = {
                difficulty:props.difficulty,
                selectedAnswer:answerOptions.current[answer],
                correctAnswer:answerOptions.current[correctAnswer.current],
                time: new Date(),
                isCorrect: isCorrectAnswer
            }
            console.log('atualizando')
            if(props.questionNumber !== 1){
                myAxios.post('/questions.json', {...data}).then(() => {getNextQuestion(); openModal()})
            }
            
        } else {
            history.push('/result')
        }
        
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.questionNumber])

    useEffect(() => {
        getNextQuestion();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    return (
        <div className="container header">
            <div className="row">
                <div className="col col-auto mr-auto">
                    <h4>{categoryName.current}</h4>
                </div>
                <div className="col col-auto align-self-center">
                <a href='/' onClick={exitHandler} className="align-self-center">
                    <div className="row exit">
                        <div className="col icon">
                            <FontAwesomeIcon icon={faTimesCircle}/>
                        </div>
                        <div className="col">
                            <p>Exit</p>
                        </div>
                    </div>
                </a>
                    
                </div>
            </div>
            
    
            <div className="card shadow-sm questions">
                <div className="card-body">                
                    <div className="container">
                        <div className="row question-header">
                            <div className="col col-auto mr-auto">
                                <h4>Question {props.questionNumber}</h4>
                            </div>
                            <div className="col col-auto">
                                {difficultyHTML.current}                            
                            </div>
                        </div>                                    
                    </div>

                
                    <span className="card-text">{questionHTML.current}
                    {props.options.map((el, index) => {
                        return  <div key={index} className={selectedAnswer === index + 1 ? "card border border-primary" : "card"}>
                                    <a href="#" onClick={() => answerSelectionHandler(index)} className="stretched-link"> <p className="card-text">{el}</p></a>
                                </div>
                        })
                    }
                    </span> 

                    <div className="card-footer">
                        <div className="row">
                            <div className="col col-auto align-self-center">
                                <a href="#" onClick={feedbackHandler} className={selectedAnswer > 0 ? `btn btn-primary align-self-center active` : `btn btn-secondary align-self-center disabled`}>Answer</a>
                            </div>
                        </div>
                    </div>

                    
                    <Modal 
                        visible={visibleModal}
                        width="300"
                        height="225"
                        effect="fadeInUp"
                    >
                        
                        <div className="container justify-content-center">
                            <div className={isCorrectAnswer ? "row align-items-center correct-icon" : "row align-items-center incorrect-icon"}>
                                <FontAwesomeIcon icon={isCorrectAnswer ? faCheckCircle : faTimesCircle}/>
                            </div>
                            <div className="row align-items-center">
                                <div className="col align-items-center">
                                    <h4 className="text-center">{ isCorrectAnswer ? `You're right!` : `You're wrong!`}</h4>
                                </div>
                                
                            </div>
                            <div className="row">
                                <a href="#" className="btn btn-primary btn-advance" onClick={() => closeModal()}>Advance <FontAwesomeIcon icon={faArrowRight}/></a>
                            </div>
                        </div>
                     
                    </Modal> 
                </div>
            </div>
        </div>
    )
}

const mapStatetoProps = state => {
    return {
        question: state.currentQuestion,
        questionNumber: state.questionNumber,
        options: state.currentOptions,
        category: state.categoryId,
        difficulty: state.difficulty,
        lastCorrect: state.lastCorrectAnswer
    }
}

const mapDispatchToProps = dispatch => {
    return {
          updateQuestion: (question, options) => dispatch({type: actionTypes.CURRENT_QUESTION, questionData:{question:question, options: options}}),
          exitTrivia: () => dispatch({type: actionTypes.EXIT}),
          raiseDifficulty: () => dispatch({type: actionTypes.RAISE_DIFFICULTY}),
          lowerDifficulty: () => dispatch({type: actionTypes.LOWER_DIFFICULTY}),
          correctQuestion: () => dispatch({type: actionTypes.CORRECT_QUESTION}),
          incorrectQuestion: () => dispatch({type: actionTypes.INCORRECT_QUESTION}),
    }
}

export default withRouter(connect(mapStatetoProps, mapDispatchToProps)(Trivia));