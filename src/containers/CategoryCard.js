/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'
import '../styles/css/CategoryCard.css'
import {useHistory  , withRouter } from 'react-router-dom'
import * as actionTypes from '../store/actions'
import { connect } from 'react-redux'

const CategoryCard = (props) => {
    let history = useHistory()
    
    const CategoryCardClickHandler = () => {    
        props.onClickCategory(props.id);    
        history.push('/trivia')
    
    }

    return (
        <React.Fragment>
            <div id="categorie-card-align" className="card shadow-sm p-3 mb-5 bg-white rounded" onClick={CategoryCardClickHandler}>
                <div id="categorie-card-align" className="card-body d-flex flex-column">
                    <div className="mt-auto">
                        <a href="#" className="stretched-link">                                                                        
                            <h5 className="card-title">{props.title}</h5>
                        </a>                                                
                    </div>
                </div>
            </div>    
        </React.Fragment>
    )
}

const mapStatetoProps = state => {
    return {
        lastCategory: state.categoryId
    }
}

const mapDispatchToProps = dispatch => {
     return {
        onClickCategory: (id) => dispatch({type: actionTypes.CURRENT_CATEGORY, categoryData:{idCategory:id}})
     }
}

export default withRouter(connect(mapStatetoProps, mapDispatchToProps)(CategoryCard));