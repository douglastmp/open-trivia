import React from 'react'

const headerColor = {
    backgroundColor: '#343c58',
    margin: 0,
    color: 'white'
}

const Header = () => {
    return (
        <nav className="navbar navbar-expand navbar-dark" style={headerColor}>
            <span className="h4">Open Trivia</span >
        </nav>
    )
}

export default Header;