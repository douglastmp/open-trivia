import React, {useEffect,useState} from 'react'
import Axios from 'axios'
import CategoryCard from '../containers/CategoryCard';
import '../styles/css/Categories.css';
import '../styles/css/CategoryCard.css'

const Categories = () => {
    const [categoriesData, setCategoriesData] = useState([]);    
    useEffect(() => {
        Axios
        .get('https://opentdb.com/api_category.php')
        .then((res) => {
            setCategoriesData(res.data.trivia_categories);
        })
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])



    const categoriesHTML = categoriesData.map((el, index) => {return <CategoryCard key={index} id={el.id} title={el.name} />});

    return (
        <div className="container">
            <div className="row title">
                <div className="container">
                    <h1>Categories</h1>
                </div>
                
            </div>
            <div className="row">
                <div className="container">
                    <div id="categorie-card" className="card-columns">
                        {categoriesHTML}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Categories;