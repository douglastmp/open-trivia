This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Dependencies
This project was build using the following libraries and their respective versions: <br />
-React - 16.10.2, <br />
-@fortawesome/fontawesome-svg-core - 1.2.25, <br />
-@fortawesome/free-solid-svg-icons - 5.11.2, <br />
-@fortawesome/react-fontawesome - 0.1.7, <br />
-address - 1.1.2, <br />
-axios - 0.19.0, <br />
-bootstrap - 4.3.1, <br />
-css-loader - 3.2.0, <br />
-firebase -7.2.1, <br />
-firebase-tools - 7.6.1, <br />
-font-awesome - 4.7.0, <br />
-node-sass - 4.12.0, <br />
-react - 16.10.2, <br />
-react-awesome-modal - 2.0.5, <br />
-react-dom - 16.10.2, <br />
-react-redux - 7.1.1, <br />
-react-router-dom - 5.1.2, <br />
-react-scripts - 3.2.0, <br />
-redux - 4.0.4, <br /> 
-sass - 1.23.0, <br />
-style-loader - 1.0.0 <br />

## Showcase

It's possible so see the final result in the link: http://douglastmp-open-trivia.35.223.243.198.nip.io/ <br />
The link contains the current version because of the auto devops tools from gitlab.


## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.


### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.


